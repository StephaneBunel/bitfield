[![pipeline status](https://gitlab.com/StephaneBunel/bitfield/badges/master/pipeline.svg)](https://gitlab.com/StephaneBunel/bitfield/-/commits/master)
[![coverage report](https://gitlab.com/StephaneBunel/bitfield/badges/master/coverage.svg)](https://gitlab.com/StephaneBunel/bitfield/-/commits/master)

# bitfield

A simple bit field manager in Go

```
package main

import (
	"fmt"

	"gitlab.com/StephaneBunel/bitfield"
)

func main() {
	// Create a new bitfield of 10 bits, not auto expandable
	bf := bitfield.New(10, bitfield.OptDefault)
	fmt.Println(bf) // 0[----------[10

	bf.SetAll()
	fmt.Println(bf) // 0[XXXXXXXXXX[10

	bf.FlipAll()
	fmt.Println(bf) // 0[----------[10

	bf.Set(1, 4, 7)
	fmt.Println(bf) // 0[-X--X--X--[10

	r := bf.Test(4)
	fmt.Println(r) // true

	r = bf.Test(5, 6)
	fmt.Println(r) // false

	r = bf.TestAll()
	fmt.Println(r) // true

	save, _ := bf.Save("JSON")
	fmt.Println(save) // {"Len":10,"Options":0,"Data":"kgA="}

	bf2 := bitfield.New(0, bitfield.OptDefault)
	bf2.Load("JSON", save)
	r = bf.Equal(bf2)
	fmt.Println(r)   // true
	fmt.Println(bf2) // 0[-X--X--X--[10
}
```


# testing

```
go test -cover bitfield
```
