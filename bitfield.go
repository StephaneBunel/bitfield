package bitfield

import (
	"encoding/json"
	"fmt"
	"strings"
)

type bitfield struct {
	// Number of bits in the bitfield
	len int

	// Behavior options
	options Options

	// The bitfield store
	data []uint8
}

func New(numbit int, opts Options) Bitfielder {
	bf := new(bitfield)
	bf.len = numbit
	bf.options = opts
	bf.data = make([]uint8, 1+(numbit/8))
	return bf
}

func (bf *bitfield) Bytes() []byte {
	return []byte(bf.data)
}

// String returns a printable representation of bitfield
func (bf *bitfield) String() string {
	repr := make([]byte, bf.len)
	for i := int(0); int(i) < bf.len; i++ {
		pos, offset := (i / 8), (i % 8)
		val, c := bf.data[pos]&(1<<offset), '-'
		if val != 0 {
			c = 'X'
		}
		repr[i] = byte(c)
	}
	return fmt.Sprintf("0[%s[%d", repr, bf.len)
}

// posOffset returns position in data and bit offset
// If index is out of range and autoExpand is true then data will be expanded.
// Returns panic(ErrIndexOutOfRange) otherwise
func (bf *bitfield) posOffset(index int) (int, int) {
	if int(index) >= bf.len {
		if bf.options&OptAutoExpand == 0 {
			panic(ErrIndexOutOfRange)
		}
		bf.len = int(index) + 1
		if index >= int(len(bf.data)*8) {
			newdata := make([]uint8, (index/8)+1)
			copy(newdata, bf.data)
			bf.data = newdata
		}
	}
	return index / 8, index % 8
}

func (bf *bitfield) Len() int {
	return bf.len
}

func (bf *bitfield) Set(indexes ...int) Bitfielder {
	for _, index := range indexes {
		pos, offset := bf.posOffset(index)
		bf.data[pos] |= (1 << offset)
	}
	return bf
}

func (bf *bitfield) SetAll() Bitfielder {
	nbytes, mod := bf.len/8, bf.len%8
	for i := 0; i < int(nbytes); i++ {
		bf.data[i] = 0xff
	}
	if mod != 0 {
		for i := int(nbytes * 8); i < int(bf.len); i++ {
			pos, offset := i/8, i%8
			bf.data[pos] |= (1 << offset)
		}
	}
	return bf
}

func (bf *bitfield) Clr(indexes ...int) Bitfielder {
	for _, index := range indexes {
		pos, offset := bf.posOffset(index)
		bf.data[pos] &= ^(1 << offset)
	}
	return bf
}

func (bf *bitfield) ClrAll() Bitfielder {
	nbytes, mod := bf.len/8, bf.len%8
	for i := 0; i < int(nbytes); i++ {
		bf.data[i] = 0x00
	}
	if mod != 0 {
		for i := int(nbytes * 8); i < int(bf.len); i++ {
			pos, offset := i/8, i%8
			bf.data[pos] &= ^(1 << offset)
		}
	}
	return bf
}

func (bf *bitfield) Equal(b Bitfielder) bool {
	lbit := bf.len
	lbyte := len(bf.data)
	if b.Len() != lbit {
		panic(ErrLenMustBeEqual)
	}
	data := b.Bytes()

	// iterate over full byte
	for i := int(0); i < (lbit / 8); i++ {
		if bf.data[i] != data[i] {
			return false
		}
	}

	// compare last n bit
	if mod := lbit % 8; mod == 0 {
		return true
	} else {
		mask := uint8(1)
		for i := int(1); i < mod; i++ {
			mask |= (1 << i)
		}
		if bf.data[lbyte-1]&mask != data[lbyte-1]&mask {
			return false
		}
	}
	return true
}

func (bf *bitfield) Test(indexes ...int) bool {
	for _, i := range indexes {
		pos, offset := bf.posOffset(i)
		if bf.data[pos]&(1<<offset) == 0 {
			return false
		}
	}
	return true
}

func (bf *bitfield) TestAll() bool {
	nbytes, mod := bf.len/8, bf.len%8
	for i := 0; i < int(nbytes); i++ {
		if bf.data[i] != 0 {
			return true
		}
	}
	if mod != 0 {
		for i := int(nbytes * 8); i < int(bf.len); i++ {
			pos, offset := i/8, i%8
			if bf.data[pos]&(1<<offset) != 0 {
				return true
			}
		}
	}
	return false
}

func (bf *bitfield) Flip(indexes ...int) Bitfielder {
	for _, i := range indexes {
		pos, offset := bf.posOffset(i)
		bm := uint8(1 << offset)
		if bf.data[pos]&bm == 0 {
			bf.data[pos] |= bm
		} else {
			bf.data[pos] &= ^bm
		}
	}
	return bf
}

func (bf *bitfield) FlipAll() Bitfielder {
	nbytes, mod := bf.len/8, bf.len%8
	for i := 0; i < int(nbytes); i++ {
		bf.data[i] = ^bf.data[i]
	}
	if mod != 0 {
		for i := int(nbytes * 8); i < int(bf.len); i++ {
			pos, offset := i/8, i%8
			bm := uint8(1 << offset)
			if bf.data[pos]&bm == 0 {
				bf.data[pos] |= bm
			} else {
				bf.data[pos] &= ^bm
			}
		}
	}
	return bf
}

func (bf *bitfield) Uint64() uint64 {
	r := uint64(0)
	for i := int(0); i < 64 && i < int(bf.len); i++ {
		pos, offset := bf.posOffset(i)
		if bf.data[pos]&uint8(1<<offset) != 0 {
			r |= uint64(1 << i)
		}
	}
	return r
}

func (bf *bitfield) Save(format string) (string, error) {
	switch strings.ToLower(format) {
	case "json":
		blob := struct {
			Len     int
			Options Options
			Data    []uint8
		}{
			bf.len,
			bf.options,
			bf.data,
		}
		r, e := json.Marshal(blob)
		return string(r), e
	}
	return "", ErrSaveFormatUnknown
}

func (bf *bitfield) Load(format string, input string) error {
	switch strings.ToLower(format) {
	case "json":
		blob := struct {
			Len     int
			Options Options
			Data    []uint8
		}{}
		if err := json.Unmarshal([]byte(input), &blob); err != nil {
			return fmt.Errorf("Error loading JSON bitfield: %w", err)
		}
		bf.len = blob.Len
		bf.options = blob.Options
		bf.data = blob.Data
		return nil
	}
	return ErrLoadFormatUnknown
}
