package bitfield

import (
	"testing"
)

func Test_NewAndLen(t *testing.T) {
	numbits := int(71)
	result := New(numbits, OptDefault).Len()
	expected := numbits
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_String(t *testing.T) {
	numbits := int(14)
	result := New(numbits, OptDefault).String()
	expected := "0[--------------[14"
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_SetAndString(t *testing.T) {
	numbits := int(14)
	result := New(numbits, OptDefault).Set(1, 4, 7).String()
	expected := "0[-X--X--X------[14"
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_SetAllAndString(t *testing.T) {
	numbits := int(14)
	result := New(numbits, OptDefault).SetAll().String()
	expected := "0[XXXXXXXXXXXXXX[14"
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_ClrAndString(t *testing.T) {
	numbits := int(14)
	result := New(numbits, OptDefault).SetAll().Clr(4).String()
	expected := "0[XXXX-XXXXXXXXX[14"
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_ClrAll(t *testing.T) {
	numbits := int(14)
	result := New(numbits, OptDefault).SetAll().ClrAll().String()
	expected := "0[--------------[14"
	if result != expected {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_Bytes(t *testing.T) {
	numbits := int(15)
	result := New(numbits, OptDefault).Set(1, 4, 7, 14).Bytes()
	expected := []byte{(1 << 1) | (1 << 4) | (1 << 7), (1 << (14 % 8))}
	if len(expected) != len(result) {
		t.Errorf("len should be %v, not %v", len(expected), len(result))
	}
	for i := 0; i < len(expected); i++ {
		if expected[i] != result[i] {
			t.Errorf("want %v, got %v", expected, result)
			break
		}
	}
}

func Test_Equal(t *testing.T) {
	numbits := int(9)
	bf1 := New(numbits, OptDefault).Set(1, 4, 7)
	bf2 := New(numbits, OptDefault).Set(1, 4, 7)
	result := bf1.Equal(bf2)
	expected := true
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	bf1.ClrAll()
	bf2.ClrAll()
	result = bf1.Equal(bf2)
	expected = true
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	bf1.Set(8)
	result = bf1.Equal(bf2)
	expected = false
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_Test(t *testing.T) {
	numbits := int(14)
	bf := New(numbits, OptDefault).Set(1, 4, 7)

	expected := true
	result := bf.Test(1)
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	expected = false
	result = bf.Test(0)
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	expected = true
	result = bf.Test(1, 4, 7)
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_TestAll(t *testing.T) {
	numbits := int(10)
	bf := New(numbits, OptDefault).Set(1, 4, 7)

	expected := true
	result := bf.TestAll()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	expected = false
	result = bf.ClrAll().TestAll()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_Flip(t *testing.T) {
	numbits := int(10)
	bf := New(numbits, OptDefault).Set(1, 4, 7).Flip(1, 4, 7)

	expected := "0[----------[10"
	result := bf.String()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	expected = "0[-X--X--X--[10"
	result = bf.Flip(1, 4, 7).String()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	expected = "0[X-XX-XX-XX[10"
	result = bf.FlipAll().String()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

}

func Test_Uint64(t *testing.T) {
	numbits := int(10)
	bf := New(numbits, OptDefault).Set(1, 4, 7, 9)

	expected := uint64(0b1010010010)
	result := bf.Uint64()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}
}

func Test_SaveJSON(t *testing.T) {
	numbits := int(10)
	bf := New(numbits, OptDefault).Set(1, 4, 7, 9)
	expected := `{"Len":10,"Options":0,"Data":"kgI="}`
	result, err := bf.Save("JSON")
	if err != nil {
		t.Errorf("got unwanted error: %v", err)
	}
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	if _, err = bf.Save("ZZ"); err != ErrSaveFormatUnknown {
		t.Errorf("want error: %v, got %v", ErrSaveFormatUnknown, err)
	}
}

func Test_LoadJSON(t *testing.T) {
	numbits := int(1)
	bf := New(numbits, OptDefault)
	err := bf.Load("JSON", `{"Len":10,"Options":0,"Data":"kgI="}`)
	if err != nil {
		t.Errorf("got unwanted error: %v", err)
	}

	expected := "0[-X--X--X-X[10"
	result := bf.String()
	if expected != result {
		t.Errorf("want %v, got %v", expected, result)
	}

	err = bf.Load("ZZ", "")
	if err != ErrLoadFormatUnknown {
		t.Errorf("got no error, want: %v", ErrLoadFormatUnknown)
	}
}
