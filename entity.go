package bitfield

import (
	"errors"
)

// Bitfielder is a bit field interface
type Bitfielder interface {
	Bytes() []byte                          // export the current bitfield in an array of byte
	Clr(...int) Bitfielder                  // Clear the bit at index
	ClrAll() Bitfielder                     // Clear all bits
	Equal(Bitfielder) bool                  // Return true if the two bitfield are equal
	Flip(...int) Bitfielder                 // Flip the bit at index
	FlipAll() Bitfielder                    // Flip all bits
	Len() int                               // Number of bits in field
	Load(format string, input string) error // Load bitfield state from formated input string
	Save(format string) (string, error)     // Save bitfield state to formated string
	Set(...int) Bitfielder                  // Set the bit at index
	SetAll() Bitfielder                     // Set all bits
	String() string                         // Returns string
	Test(...int) bool                       // Return true if bit(s) is(are) set, false otherwise
	TestAll() bool                          // Return false if bit field equal zero, true otherwise
	Uint64() uint64                         // Returns the bitfield as uint64. bitfield is truncated to 64 bits
}

// Options describe the behavior of the bit field
type Options uint16

// Errors that may be returned by this package
var (
	ErrIndexOutOfRange   = errors.New("Index is out of range")
	ErrLenMustBeEqual    = errors.New("Length of bitfield must be equal to compare them")
	ErrSaveFormatUnknown = errors.New("Unknonw format to save bitfiled")
	ErrLoadFormatUnknown = errors.New("Unknonw format to load bitfiled")
)

// New bitfield options
const (
	OptDefault    = 0
	OptAutoExpand = 1 << 0
)
